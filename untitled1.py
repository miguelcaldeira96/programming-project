if True:
    print('hello world')
if False:
    print('goodbye world')
a=True
if a:
    print('hello',a)
b=False
if b:
    print('hello',b)
    
if 1:
    print('hello',1)
if 0:
    print('hello',0)
if 2:
    print('hello',2)
if True:
    print('hello world')
def return_true():
    return True
if return_true():
    print('hello world')

val=return_true()
if val:
    print('hello world')
a=True
b=False
if a or a:
    print('at least one thing is True')
if a or b:
    print('at least on thing is True')
if b or b or b or a:
    print('at least one thing is True')
a_list=[1,2,3]
for num in a_list:
    print(num)
bool(1)

bool(1.1)

bool(0)

bool(0.0001)
bool('')
bool('None')

